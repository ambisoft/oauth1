class UserSessionsController < ApplicationController

  def new
    @user_session = UserSession.new
  end
  
  def create    
    @user_session = UserSession.new params[:user_session]
    puts @user_session.inspect
    if @user_session.save
      puts "Saved!"
      redirect_to root_path
    else
      puts @user_session.errors
      puts "Save error!"
      render 'new'
    end        
  end

  def destroy    
    current_user_session.destroy
    redirect_to root_path
  end

end
