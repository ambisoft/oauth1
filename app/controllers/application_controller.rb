class ApplicationController < ActionController::Base
  
  protect_from_forgery
  
  helper_method :current_user_session, :current_user
  
  private
  
  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find(:proxy) || UserSession.find
  end
  
  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.user
  end
  
  def require_user
    unless current_user
      store_location
      render :json => {:success => false, :errorInfo => "You must be logged in to access this page"}
    end
  end

  def require_no_user
    if current_user
      store_location
      current_user_session.destroy
    end
  end

  def store_location
    session[:return_to] = request.original_url
  end
  
end
