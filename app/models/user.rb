class User < ActiveRecord::Base
  
  attr_accessible :login, :name, :password, :password_confirmation
  
  acts_as_authentic do |c|
    c.validates_format_of_login_field_options = {:with => Authlogic::Regex.email}
    c.validate_email_field = false
  end
  
  has_many :client_applications
  has_many :tokens, :class_name => "OauthToken", :order => "authorized_at desc", 
                    :include => [:client_application]
     
end