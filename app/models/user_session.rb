class UserSession < Authlogic::Session::Base
  last_request_at_threshold 30.minutes
  secure Rails.env.production?
  httponly true
  remember_me true
  remember_me_for 3.months
end